FROM python:3.6

RUN apt-get update && \
    apt-get install -y \
    build-essential \
    cmake \
    git \
    wget \
    unzip \
    yasm \
    pkg-config \
    libswscale-dev \
    libtbb2 \
    libtbb-dev \
    libjpeg-dev \
    libpng-dev \
    libtiff-dev \
    libavformat-dev \
    libpq-dev \
    python3-dev

RUN pip install numpy
WORKDIR /
ENV OPENCV_VERSION="3.4.2"

RUN wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip \
    && unzip ${OPENCV_VERSION}.zip \
    && mkdir /opencv-${OPENCV_VERSION}/cmake_binary \
    && cd /opencv-${OPENCV_VERSION}/cmake_binary \
    && cmake -DBUILD_TIFF=ON \
    -DBUILD_opencv_java=OFF \
    -DWITH_CUDA=OFF \
    -DWITH_OPENGL=ON \
    -DWITH_OPENCL=ON \
    -DWITH_IPP=ON \
    -DWITH_TBB=ON \
    -DWITH_EIGEN=ON \
    -DWITH_V4L=ON \
    -DBUILD_TESTS=OFF \
    -DBUILD_PERF_TESTS=OFF \
    -DCMAKE_BUILD_TYPE=RELEASE \
    -DCMAKE_INSTALL_PREFIX=$(python3.6 -c "import sys; print(sys.prefix)") \
      -DPYTHON_EXECUTABLE=$(which python3.6) \
    -DPYTHON_INCLUDE_DIR=$(python3.6 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
      -DPYTHON_PACKAGES_PATH=$(python3.6 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
    .. \
    && make install \
    && rm /${OPENCV_VERSION}.zip \
    && rm -r /opencv-${OPENCV_VERSION}

RUN mkdir -p /stitching && cd stitching/
COPY stitch.cpp /stitching
ENV LD_LIBRARY_PATH ":/usr/local/lib"
RUN cd /stitching && \
    g++ stitch.cpp -I /usr/local/include/opencv2/ \
         -I /usr/local/lib/ \
         -I / \
         -L /usr/local/include/opencv2/ \
         -L /usr/local/lib/ \
         -L / \
          -lopencv_core \
          -lopencv_imgcodecs \
          -lopencv_stitching \
          -lopencv_imgproc \
          -lopencv_highgui \
          -lopencv_ml \
          -lopencv_video \
          -lopencv_features2d \
          -lopencv_calib3d \
          -lopencv_objdetect \
          -lopencv_flann \
          -lopencv_photo \
          -lopencv_dnn \
          -lopencv_shape \
          -lopencv_superres \
          -lopencv_videoio \
          -lopencv_videostab \
          -o stitch.o

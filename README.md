py3-opencv-docker
=================

This project only houses a Dockerfile, which encapsulates all the dependencies to build OpenCV.

Note: this Docker image is not optimised and can be improved upon. We are open to MR's, suggestions and any critique (as long as it's constructive, of course).

## Usage
You can either use the prebuilt image or build it yourself. 

### Prebuilt
You can pull the prebuilt docker image from here:
```bash
docker pull futurefragment/py3-opencv-docker:1.0.0
```

### Build your own
Build the docker image
```bash
docker build -t futurefragment/py3-opencv-docker:1.0.0 .
```

### Run the image
```bash
docker run -it --entrypoint=/bin/bash futurefragment/py3-opencv-docker:1.0.0 -s
```

Happy coding!
